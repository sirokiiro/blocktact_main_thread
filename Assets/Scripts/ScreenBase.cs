﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenBase : MonoBehaviour
{
    protected RectTransform mRectCanvas;

    // Start is called before the first frame update
    protected void Init()
    {
        mRectCanvas = transform.parent.GetComponent<RectTransform>();
    }
}
