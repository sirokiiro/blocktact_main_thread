﻿using System;
using System.Collections.Generic;
using System.Linq;

public class MasterMapObject
{
    private static Dictionary<int, MasterMapObject> dataDict;

    public int mId;
    public eCd mCd;
    public eType mType;
    public int mValue;
    public string mTextureName;

    public enum eCd
    {
        Floor,
        Switch,
        Block,
    }

    public enum eType
    {
        Normal,
        Hole,
        Crack,
        Magnet,
        Paint,

        //Normal,

        //Normal,
        Fixed,
        Iron,
        Slime,
    }
    
    public enum eCrackCollapseLevel
    {
        Verge,
        Danger,
        Safety,
    }

    public enum eIronFixedStatus
    {
        Fixed,
        Unfixed,
    }

    public static Dictionary<int, MasterMapObject> GetMasterMapObjectDict()
    {
        return dataDict;
    }

    public static void ParseCsvToDictionary()
    {
        //List<string[]> list = Util.ReadingCsv("mst_mapobject", 2);
        List<string[]> list = LocalDataManager.Instance.ReadingCsvFromLocal("/MasterData/mst_mapobject.csv", 1);
        Dictionary<int, MasterMapObject> dict = new Dictionary<int, MasterMapObject>();

        for (int i = 0; i < list.Count; i++)
        {
            MasterMapObject mmo = new MasterMapObject();

            mmo.mId = int.Parse(list[i][0]);
            Enum.TryParse(list[i][1], out mmo.mCd);
            Enum.TryParse(list[i][2], out mmo.mType);
            mmo.mValue = int.Parse(list[i][3]);
            mmo.mTextureName = list[i][4];

            dict.Add(mmo.mId, mmo);
        }

        dataDict = dict;
    }

    public static MasterMapObject GetBlockMasterFromFloorColor(MapObjectBase.eColor floorColor)
    {
        MasterMapObject block = new MasterMapObject();

        switch (floorColor)
        {
            case MapObjectBase.eColor.Red:
                block = dataDict[200];
                break;
            case MapObjectBase.eColor.Green:
                block = dataDict[201];
                break;
            case MapObjectBase.eColor.Blue:
                block = dataDict[202];
                break;
        }
        return block;
    }
}
