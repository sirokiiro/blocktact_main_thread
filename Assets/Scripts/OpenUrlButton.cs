﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.UI;

public class OpenUrlButton : MonoBehaviour
{
    public Text mTextUrl;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClickButtonOpenUrl()
    {
        Application.OpenURL(mTextUrl.text);
    }
}
