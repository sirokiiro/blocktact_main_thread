﻿
using UnityEngine;
using System.IO;
using System.Collections.Generic;

public class Util : MonoBehaviour
{
    /// <summary>   CSV読み込み    </summary>
    public static List<string[]> ReadingCsv(string textName,int startReadLine = 1)
    {
        List<string[]> list = new List<string[]>();

        TextAsset csvFile = Resources.Load(textName) as TextAsset;
        StringReader reader = new StringReader(csvFile.text);

        int nowReadLine = 0;

        while (reader.Peek() > -1)
        {
            string[] str = reader.ReadLine().Split(',');
            nowReadLine++;

            if (nowReadLine >= startReadLine) list.Add(str);
        }
        return list;
    }

    public static void DestroyChildObject(Transform parent_trans)
    {
        for (int i = 0; i < parent_trans.childCount; ++i)
        {
            GameObject.Destroy(parent_trans.GetChild(i).gameObject);
        }
    }
}
