﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenManager : MonoBehaviour
{
    public static ScreenManager instance;

    public enum eScreenNo
    {
        Title,
        MainMenu,
        Stage,
        StageList,
        Other,
    }

    public List<GameObject> mPrefabScreenList;
    private Dictionary<eScreenNo,GameObject> mScreenGeneratedDict;
    private eScreenNo mNowScreenNo;

    private void Awake()
    {
        //helloworld();

        instance = this;

        //FireBaseManager.Init();

        mScreenGeneratedDict = new Dictionary<eScreenNo, GameObject>();

        mNowScreenNo = eScreenNo.Title;
        mScreenGeneratedDict[eScreenNo.Title] = Instantiate(mPrefabScreenList[(int)mNowScreenNo], this.transform);
    }

    public void Transition(eScreenNo newScreenNo, params object[] param)
    {
        mScreenGeneratedDict[mNowScreenNo].gameObject.SetActive(false);

        if (mScreenGeneratedDict.ContainsKey(newScreenNo) == false)
        {
            mScreenGeneratedDict[newScreenNo] = Instantiate(mPrefabScreenList[(int)newScreenNo], this.transform);
        }
        else
        {
            //  以下に続く処理にて、アクティブになっている必要がある時があるため、先にアクティブにする
            mScreenGeneratedDict[newScreenNo].gameObject.SetActive(true);
        }

        switch (newScreenNo)
        {
            case eScreenNo.Title:
            case eScreenNo.MainMenu: break;
            case eScreenNo.Stage:
                ScreenStage stage = mScreenGeneratedDict[newScreenNo].GetComponent<ScreenStage>();
                stage.SetMapInfo(param[0] as MasterStage, (PlayerData.eClearState)param[1], (int)param[2]); 
                break;

            case eScreenNo.StageList:
                ScreenStageSelect screenStageSelect = mScreenGeneratedDict[newScreenNo].GetComponent<ScreenStageSelect>();

                for(int i = 0;i < screenStageSelect.mToggleList.Count;++i)
                {
                    if (screenStageSelect.mToggleList[i].isOn == true) screenStageSelect.CreateStageList((MasterStage.eDifficulty)i);
                }
                
                break;

            case eScreenNo.Other: break;
        }


        if (mNowScreenNo == eScreenNo.Stage)
        {
            Destroy(mScreenGeneratedDict[mNowScreenNo]);
            mScreenGeneratedDict.Remove(mNowScreenNo);
        }

        mNowScreenNo = newScreenNo;
    }
}
