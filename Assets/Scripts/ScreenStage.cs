﻿
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenStage : ScreenBase
{
    public static int mapTipNumCol;
    public static int mapTipNumLine;
    public static float mapTipSize;

    public GameObject mObjFloorParent;
    public GameObject mObjSwitchParent;
    public GameObject mObjBlockParent;

    public MapObjectFloor mObjFloorPrefab;
    public MapObjectSwitch mObjSwitchPrefab;
    public MapObjectBlock mObjBlockPrefab;

    public Text mTextStageName;
    public Text mTextGameClear;
    public Text mTextMoveTimesCount;

    private List<MapObjectFloor> mMapObjFlList = new List<MapObjectFloor>();
    private List<MapObjectSwitch> mMapObjSwList = new List<MapObjectSwitch>();
    private List<MapObjectBlock> mMapObjBkList = new List<MapObjectBlock>();

    private MasterStage mMasterStage;
    private PlayerData.eClearState mClearState;
    private int mMoveTimes;

    private List<string[]> mMapData = new List<string[]>();
    private int[,] mMapObjIdData;

    private int mMoveTimesCount;
    private bool clearflg = false;

    // Use this for initialization
    void Start()
    {
        Init();
        //SetMapInfo();
    }

    // Update is called once per frame
    void Update()
    {
        FlickManager.Instance.Flick();

        if(mBlockInvisibleing == false)
        {
            MoveBlock();
        }
    }

    /// <summary> マップ生成 </summary>
    public void SetMapInfo(MasterStage masterStage,PlayerData.eClearState clearState,int moveTimes)
    {
        Init();

        for(int i = 0;i < mMapObjFlList.Count; ++i)
        {
            Destroy(mMapObjFlList[i].gameObject);
        }

        for (int i = 0; i < mMapObjSwList.Count; ++i)
        {
            Destroy(mMapObjSwList[i].gameObject);
        }

        for (int i = 0; i < mMapObjBkList.Count; ++i)
        {
            Destroy(mMapObjBkList[i].gameObject);
        }

        mMapObjFlList = new List<MapObjectFloor>();
        mMapObjSwList = new List<MapObjectSwitch>();
        mMapObjBkList = new List<MapObjectBlock>();

        mMasterStage = masterStage;
        mClearState = clearState;
        mMoveTimes = moveTimes;

        mMoveTimesCount = 0;
        mTextStageName.text = mMasterStage.mName;
        mTextGameClear.gameObject.SetActive(false);
        mTextMoveTimesCount.text = "動かした回数 : " + 0.ToString();
        clearflg = false;

        //  ここでただの文字列のマップデータを使えるように整形する
        //mMapData = Util.ReadingCsv("stage_" + masterStage.mId.ToString());
        mMapData = masterStage.GetMapDataFormatFromStringToList();
        
        mapTipNumCol = mMapData[0].Length;
        mapTipNumLine = mMapData.Count;
        mapTipSize = mRectCanvas.rect.width * (Screen.currentResolution.width / mRectCanvas.rect.width) / ScreenStage.mapTipNumCol;

        mMapObjIdData = new int[mapTipNumCol, mapTipNumLine];
        Dictionary<int, MasterMapObject> mMasterMapObjectDict = MasterMapObject.GetMasterMapObjectDict();

        for (int i = 0; i < mapTipNumLine; ++i)
        {
            for (int j = 0; j < mapTipNumCol; ++j)
            {
                string id = mMapData[mapTipNumLine - 1 - i][j];
                MasterMapObject masterMapObject = mMasterMapObjectDict[int.Parse(id)];
                MasterMapObject.eCd mapobjectCd = masterMapObject.mCd;


                MapObjectFloor mapObjectFloorDefault = Instantiate(mObjFloorPrefab);
                mapObjectFloorDefault.gameObject.transform.SetParent(mObjFloorParent.gameObject.transform);
                mapObjectFloorDefault.SetInfo(mMasterMapObjectDict[000], j, i);
                mMapObjFlList.Add(mapObjectFloorDefault);


                switch (mapobjectCd)
                {
                    case MasterMapObject.eCd.Floor:
                        if (id == "000") continue;
                        MapObjectFloor mapObjectFloor = Instantiate(mObjFloorPrefab);
                        mapObjectFloor.gameObject.transform.SetParent(mObjFloorParent.gameObject.transform);
                        mapObjectFloor.SetInfo(masterMapObject, j, i);
                        mMapObjFlList.Add(mapObjectFloor);
                        break;

                    case MasterMapObject.eCd.Switch:
                        MapObjectSwitch mapObjectSwitch = Instantiate(mObjSwitchPrefab);
                        mapObjectSwitch.gameObject.transform.SetParent(mObjSwitchParent.gameObject.transform);
                        mapObjectSwitch.SetInfo(masterMapObject, j, i);
                        mMapObjSwList.Add(mapObjectSwitch);
                        break;

                    case MasterMapObject.eCd.Block:
                        MapObjectBlock mapObjectBlock = Instantiate(mObjBlockPrefab);
                        mapObjectBlock.gameObject.transform.SetParent(mObjBlockParent.gameObject.transform);
                        mapObjectBlock.SetInfo(masterMapObject, j, i);
                        mMapObjBkList.Add(mapObjectBlock);
                        mMapObjIdData[j, i] = mapObjectBlock.gameObject.GetInstanceID();
                        break;
                }
            }
        }
    }
    
    private void MoveBlock()
    {
        if (clearflg == true) return;

        if(FlickManager.Instance.GetDirection() == FlickManager.eDirection.None
            || FlickManager.Instance.GetDirection() == FlickManager.eDirection.Touch)
        {
            return;
        }

        switch (FlickManager.Instance.GetDirection())
        {
            case FlickManager.eDirection.Up:
                //  Y値の降順ソート
                mMapObjBkList.Sort((a, b) => b.GetPositionLine() - a.GetPositionLine());
                break;
            case FlickManager.eDirection.Down:
                //  Y値の昇順ソート
                mMapObjBkList.Sort((a, b) => a.GetPositionLine() - b.GetPositionLine());
                break;
            case FlickManager.eDirection.Left:
                //  X値の昇順ソート
                mMapObjBkList.Sort((a, b) => a.GetPositionCol() - b.GetPositionCol());
                break;
            case FlickManager.eDirection.Right:
                //  X値の降順ソート
                mMapObjBkList.Sort((a, b) => b.GetPositionCol() - a.GetPositionCol());
                break;
        }

        bool IsExistMoveBlock = false;

        int destPosCol = 0;
        int destPosLine = 0;

        for (int i = 0; i < mMapObjBkList.Count; ++i)
        {
            MapObjectBlock block = mMapObjBkList[i];

            if (block.GetMaster().mType == MasterMapObject.eType.Fixed
                || (block.GetMaster().mType == MasterMapObject.eType.Iron && block.GetMaster().mValue == (int)MasterMapObject.eIronFixedStatus.Fixed))
            {
                continue;
            }

            //  移動予定位置設定
            switch (FlickManager.Instance.GetDirection())
            {
                case FlickManager.eDirection.Up:
                    destPosCol = block.GetPositionCol();
                    destPosLine = block.GetPositionLine() + 1;
                    break;
                case FlickManager.eDirection.Down:
                    destPosCol = block.GetPositionCol();
                    destPosLine = block.GetPositionLine() - 1;
                    break;
                case FlickManager.eDirection.Left:
                    destPosCol = block.GetPositionCol() - 1;
                    destPosLine = block.GetPositionLine();
                    break;
                case FlickManager.eDirection.Right:
                    destPosCol = block.GetPositionCol() + 1;
                    destPosLine = block.GetPositionLine();
                    break;
            }

            //DebugManager.instance.log("destposcol  " + destposcol);
            //debugmanager.instance.log("destposline  " + destposline);
            //debugmanager.Instance.Log("mMapObjIdData[destPosCol,destPosLine]  " + mMapObjIdData[destPosCol, destPosLine]);

            //  移動判定
            if (destPosCol >= 0
                && destPosCol < mapTipNumCol
                && destPosLine >= 0
                && destPosLine < mapTipNumLine
                && mMapObjIdData[destPosCol,destPosLine] == 0
                )
            {
                //DebugManager.Instance.Log("move!! id  " + block.GetMaster().mId);
                mMapObjIdData[block.GetPositionCol(),block.GetPositionLine()] = 0;
                mMapObjIdData[destPosCol,destPosLine] = block.gameObject.GetInstanceID();

                block.SetPosition(destPosCol, destPosLine);
                
                IronBlockFixedByMagnet(block);
                ChangeBlockColorByPaintFloor(block);
                CrackFloorCollapseIsProgress(block);
                if (DownBlockByHole(block) == true) --i;
                
                IsExistMoveBlock = true;
            }
        }

        if (IsExistMoveBlock == true)
        {
            mMoveTimesCount++;
            mTextMoveTimesCount.text = "動かした回数 : " + mMoveTimesCount.ToString();
            CheckGameClear();
        }
    }

    private void IronBlockFixedByMagnet(MapObjectBlock block)
    {
        if (block.GetMaster().mType != MasterMapObject.eType.Iron) return;
        List<MapObjectFloor> crackFloorList = mMapObjFlList.FindAll(m => m.GetMaster().mType == MasterMapObject.eType.Magnet);

        for (int i = 0; i < crackFloorList.Count; ++i)
        {
            if (block.GetPositionCol() == crackFloorList[i].GetPositionCol()
                && block.GetPositionLine() == crackFloorList[i].GetPositionLine())
            {
                block.SetBlockAfterFixedByMagnet();
                break;
            }
        }
    }

    private void CrackFloorCollapseIsProgress(MapObjectBlock block)
    {
        List<MapObjectFloor> crackFloorList = mMapObjFlList.FindAll(m => m.GetMaster().mType == MasterMapObject.eType.Crack);

        for (int i = 0; i < crackFloorList.Count; ++i)
        {
            if (block.GetPositionCol() == crackFloorList[i].GetPositionCol()
                && block.GetPositionLine() == crackFloorList[i].GetPositionLine())
            {
                crackFloorList[i].SetCrackFloorAfterCollapseIsProgress();
                break;
            }
        }
    }

    private bool DownBlockByHole(MapObjectBlock block)
    {
        List<MapObjectFloor> holeFloorList = mMapObjFlList.FindAll(m => m.GetMaster().mType == MasterMapObject.eType.Hole);
        bool IsExistDownBlock = false;
        
        for (int i = 0; i < holeFloorList.Count; ++i)
        {
            if (block.GetPositionCol() == holeFloorList[i].GetPositionCol()
                && block.GetPositionLine() == holeFloorList[i].GetPositionLine())
            {
                mMapObjIdData[block.GetPositionCol(), block.GetPositionLine()] = 0;
                mMapObjBkList.Remove(block);
                Destroy(block.gameObject);

                IsExistDownBlock = true;
                break;
            }
        }

        return IsExistDownBlock;
    }

    private void ChangeBlockColorByPaintFloor(MapObjectBlock block)
    {
        List<MapObjectFloor> paintFloorList = mMapObjFlList.FindAll(m => m.GetMaster().mType == MasterMapObject.eType.Paint);

        for(int i = 0;i < paintFloorList.Count; ++i)
        {
            if(block.GetPositionCol() == paintFloorList[i].GetPositionCol()
                && block.GetPositionLine() == paintFloorList[i].GetPositionLine())
            {
                block.SetColor(paintFloorList[i].GetColor());
            }
        }

    }

    /// <summary> ゲームクリア条件を満たしているか判定 </summary>
    private void CheckGameClear()
    {
        int count = 0;
        for (int i = 0; i < mMapObjBkList.Count; ++i)
        {
            mMapObjBkList[i].AnimStop("BlockOnSwitch");

            for (int j = 0; j < mMapObjSwList.Count; ++j)
            {
                if (mMapObjSwList[j].GetColor() == mMapObjBkList[i].GetColor()
                    && mMapObjSwList[j].GetPositionCol() == mMapObjBkList[i].GetPositionCol()
                    && mMapObjSwList[j].GetPositionLine() == mMapObjBkList[i].GetPositionLine()
                    )
                {
                    mMapObjBkList[i].AnimPlay("BlockOnSwitch");
                    count++;
                    break;
                }
            }
        }

        clearflg = mMapObjSwList.Count == count;

        if (clearflg == true)
        {
            mTextGameClear.gameObject.SetActive(clearflg);
            PlayerData data = LocalDataManager.Instance.Get<PlayerData>(PlayerData.key);

            PlayerData.ClearStage clearStage;
            clearStage.id = mMasterStage.mId;
            clearStage.moveTimes = mMoveTimesCount;

            data.SetClearStage(clearStage);

            LocalDataManager.Instance.Set(PlayerData.key, data);
            LocalDataManager.Instance.WriteFile(PlayerData.key);
        }
    }

    public void OnClickButtonGoToStageSelect()
    {
        ScreenManager.instance.Transition(ScreenManager.eScreenNo.StageList);
    }

    public void OnClickButtonRetry()
    {
        SetMapInfo(mMasterStage, mClearState,mMoveTimes);
    }

    private bool mBlockInvisibleing = false;
    public void PointerDownBlockInvisible()
    {
        mBlockInvisibleing = true;
        foreach (MapObjectBlock block in mMapObjBkList) block.gameObject.SetActive(false);
    }
    public void PointerUpBlockInvisible()
    {
        mBlockInvisibleing = false;
        foreach (MapObjectBlock block in mMapObjBkList) block.gameObject.SetActive(true);
    }
}
