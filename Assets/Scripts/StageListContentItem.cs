﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageListContentItem : MonoBehaviour
{
    public Text mTextMapSize;
    public Text mTextName;
    public Text mTextDescription;
    public Text mTextDifficult;
    public Text mTextBestMoveTimesUser;
    public Text mTextBestMoveTimesCreator;
    public Text mTextClear;

    private MasterStage mMasterStage;
    private PlayerData.eClearState mClearState;
    private int mMoveTimes;

    public void SetInfo(MasterStage masterStage,int dataMoveTimes)
    {
        mMasterStage = masterStage;
        mMoveTimes = dataMoveTimes;

        if (dataMoveTimes == 0)
        {
            mClearState = PlayerData.eClearState.UnClear;
        }
        else if (dataMoveTimes <= masterStage.mBestMoveTimes)
        {
            mClearState = PlayerData.eClearState.EXCELLENT;
        }
        else
        {
            mClearState = PlayerData.eClearState.CLEAR;
        }

        SetUIInfo();
    }

    public void SetUIInfo()
    {
        mTextMapSize.text = mMasterStage.mMapSize.ToString();
        mTextName.text = mMasterStage.mName;
        mTextDescription.text = mMasterStage.mDescription;
        mTextDifficult.text = mMasterStage.mDifficulty.ToString();
        mTextBestMoveTimesUser.text = "user : " + mMoveTimes.ToString() + "回";
        mTextBestMoveTimesCreator.text = "creator : " + mMasterStage.mBestMoveTimes.ToString() + "回";

        mTextClear.gameObject.SetActive(mClearState != PlayerData.eClearState.UnClear);
        mTextClear.text = mClearState.ToString();
    }

    public void OnClickButtonGoToStage()
    {
        ScreenManager.instance.Transition(ScreenManager.eScreenNo.Stage, mMasterStage, mClearState, mMoveTimes);
    }
}
