﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenTitle : ScreenBase
{
    public GameObject mPopup;
    void Awake()
    {
        LocalDataManager.Instance.CreateRequiredDirectorys();

        if (LocalDataManager.Instance.ReadFile<PlayerData>(PlayerData.key) == false)
        {
            PlayerData data = new PlayerData();
            LocalDataManager.Instance.Set(PlayerData.key, data);
            LocalDataManager.Instance.WriteFile(PlayerData.key);
        }

        FirebaseManager.Instance.Init();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (FirebaseManager.downloadCount == 0 && FirebaseManager.downloadState == FirebaseManager.eDownloadState.Progress)
        {
            DebugManager.Instance.Log("download count " + FirebaseManager.downloadCount);
            FirebaseManager.downloadState = FirebaseManager.eDownloadState.Success;
        }

        if (FirebaseManager.downloadState == FirebaseManager.eDownloadState.Faulted)
        {
            FirebaseManager.downloadState = FirebaseManager.eDownloadState.None;
            FirebaseManager.downloadCount = 0;

            PlayerData data = LocalDataManager.Instance.Get<PlayerData>(PlayerData.key);
            if (data.mDownloadData.version == 0)
            {
                //  初回ダウンロード失敗時のみポップアップを出して遷移できないようにする
                Instantiate(mPopup, gameObject.transform);
            }
            else
            {
                ScreenManager.instance.Transition(ScreenManager.eScreenNo.MainMenu);
                MasterMapObject.ParseCsvToDictionary();
            }
        }

        if (FirebaseManager.downloadState == FirebaseManager.eDownloadState.Success)
        {
            PlayerData data = LocalDataManager.Instance.Get<PlayerData>(PlayerData.key);
            data.mDownloadData.version = 1;
            LocalDataManager.Instance.Set(PlayerData.key, data);
            LocalDataManager.Instance.WriteFile(PlayerData.key);

            FirebaseManager.downloadState = FirebaseManager.eDownloadState.None;
            FirebaseManager.downloadCount = 0;
            ScreenManager.instance.Transition(ScreenManager.eScreenNo.MainMenu);
            MasterMapObject.ParseCsvToDictionary();
        }
    }

    public void OnClickButtonGoToMainMenu()
    {
        if (FirebaseManager.downloadState == FirebaseManager.eDownloadState.None)
        {
            PlayerData data = LocalDataManager.Instance.Get<PlayerData>(PlayerData.key);
            if (data.mDownloadData.version == 0)
            {
                FirebaseManager.downloadState = FirebaseManager.eDownloadState.Progress;
                FirebaseManager.downloadCount = 0;
                FirebaseManager.Instance.DownloadMasterData();
            }
            else
            {
                // ネットワークの状態を出力
                switch (Application.internetReachability)
                {
                    case NetworkReachability.NotReachable:
                        DebugManager.Instance.Log("ネットワークには到達不可");
                        ScreenManager.instance.Transition(ScreenManager.eScreenNo.MainMenu);
                        MasterMapObject.ParseCsvToDictionary();
                        break;

                    case NetworkReachability.ReachableViaCarrierDataNetwork:
                    case NetworkReachability.ReachableViaLocalAreaNetwork:
                        FirebaseManager.downloadState = FirebaseManager.eDownloadState.Progress;
                        FirebaseManager.downloadCount = 0;
                        FirebaseManager.Instance.DownloadMasterData();
                        DebugManager.Instance.Log("ネットワークに到達可能");
                        break;
                }
            }
        }
    }
}
