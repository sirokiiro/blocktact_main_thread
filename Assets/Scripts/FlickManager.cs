﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
//using UnityEngine.iOS;

public class FlickManager
{
    // インスタンス
    private static FlickManager mInstance;

    // インスタンス取得
    public static FlickManager Instance
    {
        get
        {
            return mInstance = mInstance == null ? new FlickManager() : mInstance;
        }
    }
    public eDirection Direction;
    public enum eDirection
    {
        None,
        Touch,
        Up,
        Down,
        Left,
        Right
    }
    private Vector3 touchStartPos;
    private Vector3 touchEndPos;

    private Vector3 mOldDragAnchorPos;
    private float mDragRangeMoveBlock;

    void Start()
    {
        
    }

    public void Flick()
    {
        mDragRangeMoveBlock = 100f;
        Direction = eDirection.None;
        Vector3 nowDragPos = Input.mousePosition;
        

        if (Input.GetMouseButtonDown(0))
        {
            mOldDragAnchorPos = nowDragPos;
        }

        if (Input.GetMouseButton(0))
        {
            float distance = Vector3.Distance(nowDragPos, mOldDragAnchorPos);
            if (distance >= mDragRangeMoveBlock)
            {
                SetDirection(mOldDragAnchorPos, nowDragPos);
                mOldDragAnchorPos = nowDragPos;
            }
        }
    }

    public eDirection GetDirection()
    {
        return Direction;
    }

    void SetDirection(Vector3 pos1,Vector3 pos2)
    {
        float directionX = pos2.x - pos1.x;
        float directionY = pos2.y - pos1.y;

        if (Mathf.Abs(directionY) < Mathf.Abs(directionX)){
            if (30 < directionX)
            {
                //右向きにフリック
                Direction = eDirection.Right;
            }
            else if (-30 > directionX)
            {
                //左向きにフリック
                Direction = eDirection.Left;
            }
        }else if (Mathf.Abs(directionX) < Mathf.Abs(directionY)){
            if (30 < directionY)
            {
                //上向きにフリック
                Direction = eDirection.Up;
            }
            else if (-30 > directionY)
            {
                //下向きのフリック
                Direction = eDirection.Down;
            }
        } else {
            //タッチを検出
            Direction = eDirection.Touch;
        }
    }

    public Vector3 GetNowPosition()
    {
        Vector3 vector3 = Input.mousePosition;
        Vector2 vector2 = new Vector2(vector3.x, vector3.y);
        return vector3;
    }
}
