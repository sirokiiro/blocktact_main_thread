﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using Firebase.Storage;

public class FirebaseManager
{
    private static FirebaseManager mInstance;

    // インスタンス取得
    public static FirebaseManager Instance
    {
        get
        {
            return mInstance = mInstance == null ? new FirebaseManager() : mInstance;
        }
    }

    public static readonly string rootReference = "gs://blocktact-release.appspot.com/";
    public static readonly string masterMapobjectReference = rootReference + "MasterData/" + "mst_mapobject.csv";
    public static readonly string masterStageReference = rootReference + "MasterData/" + "mst_stage.csv";

    public enum eDownloadState
    {
        Progress,
        Success,
        Faulted,
        None,
    }
    public static eDownloadState downloadState = eDownloadState.None;
    public static int downloadCount = 0;

    //  FirebaseSDK を使用する前に最新版にしておく必要があります。
    //  最新バージョンへの更新
    public void Init()
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // 使用する準備が整った時に通る
                DebugManager.Instance.Log("すべてのFirebase依存関係を解決して、使用する準備が整いました",DebugManager.eType.INFO);
            }
            else
            {
                DebugManager.Instance.Log(String.Format("すべてのFirebase依存関係を解決できませんでした: {0}", dependencyStatus), DebugManager.eType.INFO);
            }
        });
    }

    /// <summary>
    /// FirebaseのStorageから端末のローカルにデータをダウンロードする
    /// </summary>
    /// <param name="originDownloadPath">ダウンロード元のフルパス</param>
    /// <param name="targetDownloadPath">ダウンロード先のフルパス</param>
    public void Download(string originDownloadPath, string targetDownloadPath)
    {
        downloadCount++;
        DebugManager.Instance.Log("ダウンロード元 : " + originDownloadPath + "\n" + "ダウンロード先 : " + targetDownloadPath, DebugManager.eType.INFO);

        FirebaseStorage storage = FirebaseStorage.DefaultInstance;
        StorageReference reference = storage.GetReferenceFromUrl(originDownloadPath);

        //  ダウンロード中
        Task task = reference.GetFileAsync
            (
            targetDownloadPath,
            new StorageProgress<DownloadState>((DownloadState state) =>
            {
                DebugManager.Instance.Log(String.Format("Progress: {0} of {1} bytes transferred.",
                    state.BytesTransferred,
                    state.TotalByteCount)
                    );
            }), CancellationToken.None);

        task.ContinueWith(resultTask =>
        {
            if (!resultTask.IsFaulted && !resultTask.IsCanceled)
            {
                downloadCount--;
                DebugManager.Instance.Log("Download finished.");
            }
            else
            {
                downloadState = eDownloadState.Faulted;
                DebugManager.Instance.Log("download not start");
            }
        });
    }

    public void DownloadMasterData()
    {
        Download(masterMapobjectReference, LocalDataManager.Instance.RootPath + "/MasterData/" + "mst_mapobject.csv");
        Download(masterStageReference, LocalDataManager.Instance.RootPath + "/MasterData/" + "mst_stage.csv");
    }
}
