﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenStageSelect : ScreenBase
{
    public GameObject mObjStageListContentItem;
    public GameObject mObjStageListContentItemsParent;

    public List<Toggle> mToggleList;

    // Start is called before the first frame update
    void Start()
    {
        Init();
        mToggleList[(int)MasterStage.eDifficulty.Tutorial].isOn = true;
        CreateStageList(MasterStage.eDifficulty.Tutorial);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateStageList(MasterStage.eDifficulty createTargetDifficulty)
    {
        for (int i = 0; i < mObjStageListContentItemsParent.transform.childCount; ++i)
        {
            GameObject.Destroy(mObjStageListContentItemsParent.transform.GetChild(i).gameObject);
        }

        List<MasterStage> masterStageList = MasterStage.GetMasterStageList(createTargetDifficulty);
        PlayerData data = LocalDataManager.Instance.Get<PlayerData>(PlayerData.key);

        for (int i = 0; i < masterStageList.Count; ++i)
        {
            GameObject gameobject = Instantiate(mObjStageListContentItem, mObjStageListContentItemsParent.transform);
            StageListContentItem item = gameobject.GetComponent<StageListContentItem>();

            int moveTimes = data.mClearStages.Find(m => m.id == masterStageList[i].mId).moveTimes;
            item.SetInfo(masterStageList[i], moveTimes);
        }
    }

    public void OnClickButtonGoToStage(StageListContentItem item)
    {
        ScreenManager.instance.Transition(ScreenManager.eScreenNo.MainMenu);
    }

    public void OnValueChangedToggleTutorial()
    {

        if (mToggleList[(int)MasterStage.eDifficulty.Tutorial].isOn == true)
        {
            CreateStageList(MasterStage.eDifficulty.Tutorial);
        }
    }

    public void OnValueChangedToggleEasy()
    {
        if (mToggleList[(int)MasterStage.eDifficulty.Easy].isOn == true)
        {
            CreateStageList(MasterStage.eDifficulty.Easy);
        }
    }

    public void OnValueChangedToggleNormal()
    {
        if (mToggleList[(int)MasterStage.eDifficulty.Normal].isOn == true)
        {
            CreateStageList(MasterStage.eDifficulty.Normal);
        }
    }

    public void OnValueChangedToggleHard()
    {
        if (mToggleList[(int)MasterStage.eDifficulty.Hard].isOn == true)
        {
            CreateStageList(MasterStage.eDifficulty.Hard);
        }
    }

    public void OnClickButtonGoToTitle()
    {
        ScreenManager.instance.Transition(ScreenManager.eScreenNo.MainMenu);
    }
}
