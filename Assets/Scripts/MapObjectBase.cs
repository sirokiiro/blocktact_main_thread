﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;

public class MapObjectBase : MonoBehaviour
{
    protected MasterMapObject mMaster;

    public enum eColor
    {
        Red,
        Green,
        Blue,
        Unique = 9,
    }

    protected eColor mColor;
    protected float mSize;
    protected float mSizeRate;
    protected float mScale;

    [SerializeField] protected int mCol;
    [SerializeField] protected int mLine;

    protected SpriteAtlas mAtlas;

    public MasterMapObject GetMaster() { return mMaster; }
    public int GetPositionCol() { return mCol; }
    public int GetPositionLine() { return mLine; }
    public eColor GetColor() { return mColor; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void SetInfo(MasterMapObject masterMapObject, int col, int line)
    {
        mMaster = masterMapObject;
        
        mAtlas = Resources.Load<SpriteAtlas>("MapTip");
        GetComponent<Image>().sprite = mAtlas.GetSprite(mMaster.mTextureName);

        switch (mMaster.mCd)
        {
            case MasterMapObject.eCd.Floor:
                mSizeRate = 0.97f;
                break;
            case MasterMapObject.eCd.Switch:
                mSizeRate = 0.40f;
                break;
            case MasterMapObject.eCd.Block:
                mSizeRate = 1.00f;
                break;
        }

        switch (mMaster.mType)
        {
            case MasterMapObject.eType.Normal:
            case MasterMapObject.eType.Paint:
                Enum.TryParse(mMaster.mValue.ToString(), out mColor);
                break;

            default:
                mColor = eColor.Unique;
                break;
        }

        RectTransform rect = GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(ScreenStage.mapTipSize * mSizeRate, ScreenStage.mapTipSize * mSizeRate);
        mSize = rect.sizeDelta.x;
        mScale = rect.localScale.x;

        mCol = col;
        mLine = line;
        SetPosition(col, line);
    }

    public void SetPosition(int col, int line)
    {
        mCol = col;
        mLine = line;

        float size = ScreenStage.mapTipSize * mScale;

        float offset = size / 2;
        float offsetMapX = ScreenStage.mapTipNumCol * size / 2;
        float offsetMapY = ScreenStage.mapTipNumLine * size / 2;

        float x = col  * size + offset - offsetMapX;
        float y = line * size + offset - offsetMapY;

        GetComponent<RectTransform>().localPosition = new Vector2(x, y);
    }
}
