﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;
using System.Collections.Generic;
using System.Linq;

public class MapObjectBlock : MapObjectBase
{
    public Animator mAnimator;

    public override void SetInfo(MasterMapObject mmo, int col, int line)
    {
        base.SetInfo(mmo, col, line);
        mAnimator.enabled = false;
    }

    public void SetColor(eColor color)
    {
        mMaster = MasterMapObject.GetBlockMasterFromFloorColor(color);
        mColor = color;
        GetComponent<Image>().sprite = mAtlas.GetSprite(mMaster.mTextureName);
    }

    public void SetBlockAfterFixedByMagnet()
    {
        MasterMapObject.eIronFixedStatus fixedStatus;
        Enum.TryParse(mMaster.mValue.ToString(), out fixedStatus);

        switch (fixedStatus)
        {
            case MasterMapObject.eIronFixedStatus.Unfixed:
                mMaster = MasterMapObject.GetMasterMapObjectDict()[220];
                break;
        }

        GetComponent<Image>().sprite = mAtlas.GetSprite(mMaster.mTextureName);
    }

    public void AnimPlay(string animName)
    {
        mAnimator.enabled = true;
        mAnimator.Play(animName);
    }

    public void AnimStop(string animName)
    {
        mAnimator.enabled = false;
        Color color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g, GetComponent<Image>().color.b, 1.0f);
        GetComponent<Image>().color = color;
    }
}
