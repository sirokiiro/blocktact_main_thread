﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;
using System.Text;

public class LocalDataManager
{
    private static LocalDataManager mInstance;

    // インスタンス取得
    public static LocalDataManager Instance
    {
        get
        {
            return mInstance = mInstance == null ? new LocalDataManager() : mInstance;
        }
    }

    static Dictionary<string, object> temporary = new Dictionary<string, object>();

    static readonly string path = "SaveData";
    static readonly string fullPath = $"{ Application.persistentDataPath }/{ path }";
    static readonly string rootPath = Application.persistentDataPath;
    static readonly string extension = "dat";

    public string SavePath
    {
        get
        {
            return fullPath;
        }
    }

    public string RootPath
    {
        get
        {
            return rootPath;
        }
    }

    public bool ExistsKey(string key)
    {
        return temporary.ContainsKey(key);
    }

    public void Set(string key, object obj)
    {
        temporary[key] = obj;
    }

    public DataType Get<DataType>(string key)
    {
        if (ExistsKey(key))
        {
            return (DataType)temporary[key];
        }
        else
        {
            return default(DataType);
        }
    }
    public void CreateRequiredDirectorys()
    {
        CreateDirectory(fullPath + "/" + "SaveData");
        CreateDirectory(rootPath + "/" + "MasterData");
    }

    public void CreateDirectory(string createDirectoryName)
    {
        if (Directory.Exists(createDirectoryName) == false)
        {
            Directory.CreateDirectory(createDirectoryName);
        }
    }

    public bool WriteFile(string fileName)
    {
        if (!ExistsKey(fileName))
        {
            return false;
        }

        string filePath = $"{ fullPath }/{ fileName }.{ extension }";
        string json = JsonUtility.ToJson(temporary[fileName]);
        byte[] data = Encoding.UTF8.GetBytes(json);

        if (!Directory.Exists(fullPath))
        {
            Directory.CreateDirectory(fullPath);
        }

        using (FileStream fileStream = File.Create(filePath))
        {
            fileStream.Write(data, 0, data.Length);
        }

        return true;
    }

    public bool ReadFile<DataType>(string fileName)
    {
        string filePath = $"{ fullPath }/{ fileName }.{ extension }";

        if (!File.Exists(filePath))
        {
            return false;
        }

        byte[] data = null;
        using (FileStream fileStream = File.OpenRead(filePath))
        {
            data = new byte[fileStream.Length];
            fileStream.Read(data, 0, data.Length);
        }

        string json = Encoding.UTF8.GetString(data);

        temporary[fileName] = JsonUtility.FromJson<DataType>(json);

        return true;
    }

    public List<string[]> ReadingCsvFromLocal(string targetPathFromRoot, int startReadLineIndex = 0)
    {
        List<string[]> stringsList = new List<string[]>();

        // ファイルパス指定
        StreamReader streamReader = new StreamReader(rootPath + targetPathFromRoot);

        int nowReadLineIndex = 0;

        while (!streamReader.EndOfStream)
        {
            string line = streamReader.ReadLine();
            string[] values = line.Split(',');

            if (nowReadLineIndex >= startReadLineIndex) stringsList.Add(values);
            nowReadLineIndex++;
        }
        DebugManager.Instance.StringsListLog(stringsList);
        return stringsList;
    }

    //public List<string[]> ReadingMapDataCsv(int targetStageId, int startReadLineIndex = 0)
    //{
    //    return ReadingCsv("MasterData/" + "MapData/" + targetStageId.ToString(), startReadLineIndex);
    //}

}