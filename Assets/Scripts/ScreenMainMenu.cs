﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenMainMenu : ScreenBase
{
    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void OnClickButtonGoToStageSelect()
    {
        ScreenManager.instance.Transition(ScreenManager.eScreenNo.StageList);
    }

    public void OnClickButtonGoToOther()
    {
        ScreenManager.instance.Transition(ScreenManager.eScreenNo.Other);
    }
}
