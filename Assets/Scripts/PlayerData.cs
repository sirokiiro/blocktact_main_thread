﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    [System.Serializable]
    public struct DownloadData
    {
        public int version;
    }

    [System.Serializable]
    public struct ClearStage
    {
        public int id;
        public int moveTimes;
    }
    
    public enum eClearState
    {
        UnClear,
        CLEAR,
        EXCELLENT,
    }

    public static string key = "playerdata";
    public DownloadData mDownloadData = new DownloadData();
    public List<ClearStage> mClearStages = new List<ClearStage>();

    public void SetClearStage(ClearStage newClearStageData)
    {
        bool stageIdExistsAlready = mClearStages.Exists(m => m.id == newClearStageData.id);

        if (stageIdExistsAlready == true)
        {
            ClearStage nowClearStageData;
            nowClearStageData = mClearStages.Find(m => m.id == newClearStageData.id);

            if (nowClearStageData.moveTimes > newClearStageData.moveTimes)
            {
                int index = mClearStages.FindIndex(m => m.id == newClearStageData.id);
                mClearStages[index] = newClearStageData;
            }
        }
        else
        {
            mClearStages.Add(newClearStageData);
        }
    }

    public override string ToString()
    {
        return $"{base.ToString()}{JsonUtility.ToJson(this)}";
    }

}
