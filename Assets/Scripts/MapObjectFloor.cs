﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;

public class MapObjectFloor : MapObjectBase
{
    public override void SetInfo(MasterMapObject masterMapObject, int col, int line)
    {
        base.SetInfo(masterMapObject, col, line);
    }

    public void SetCrackFloorAfterCollapseIsProgress()
    {
        MasterMapObject.eCrackCollapseLevel crackCollapseLevel;
        Enum.TryParse(mMaster.mValue.ToString(), out crackCollapseLevel);

        switch (crackCollapseLevel)
        {
            case MasterMapObject.eCrackCollapseLevel.Safety:
                mMaster = MasterMapObject.GetMasterMapObjectDict()[021];
                break;
            case MasterMapObject.eCrackCollapseLevel.Danger:
                mMaster = MasterMapObject.GetMasterMapObjectDict()[020];
                break;
            case MasterMapObject.eCrackCollapseLevel.Verge:
                mMaster = MasterMapObject.GetMasterMapObjectDict()[010];
                break;
        }

        GetComponent<Image>().sprite = mAtlas.GetSprite(mMaster.mTextureName);
    }
}
