﻿using System;
using System.Collections.Generic;
using System.IO;

public class MasterStage
{
    public enum eDifficulty
    {
        Tutorial,   //  チュートリアル
        Easy,       //  初見クリア余裕
        Normal,     //  Easyクリア者は数分でクリア
        Hard,       //  Normalクリア者は、10分程度でクリア
        Lunatic,    //  Hardクリア者でも、日を跨いでなんとかクリア
    }

    public enum eMapSize
    {
        Small  = 5, //  5*5
        Medium = 7, //  7*7
        Large  = 9, //  9*9
    }

    public int          mId;                //  ID
    public string       mName;              //  名前
    public string       mDescription;       //  説明文
    public eDifficulty  mDifficulty;        //  難易度
    public int          mMapSize;           //  大きさ
    public int          mBestMoveTimes;     //  最適な移動回数
    public string       mMapData;           //  マップデータ

    public static List<MasterStage> GetMasterStageList()
    {
        //List<string[]> stagelist = Util.ReadingCsv("mst_stagelist", 2);
        List<string[]> stagelist = LocalDataManager.Instance.ReadingCsvFromLocal("/MasterData/mst_stage.csv", 1);
        List<MasterStage> masterStageList = new List<MasterStage>();

        for (int i = 0; i < stagelist.Count; ++i)
        {
            MasterStage masterStage = new MasterStage();

            masterStage.mId = int.Parse(stagelist[i][0]);
            masterStage.mName = stagelist[i][1];
            masterStage.mDescription = stagelist[i][2];
            Enum.TryParse(stagelist[i][3], out masterStage.mDifficulty);
            masterStage.mMapSize = int.Parse(stagelist[i][4]);
            masterStage.mBestMoveTimes = int.Parse(stagelist[i][5]);
            masterStage.mMapData = stagelist[i][6];

            masterStageList.Add(masterStage);
        }

        return masterStageList;
    }

    public static List<MasterStage> GetMasterStageList(eDifficulty targetDifficulty)
    {
        List<MasterStage> masterStageList = GetMasterStageList();
        return masterStageList.FindAll(m => m.mDifficulty == targetDifficulty);
    }

    public eMapSize GetMapSizeName()
    {
        int mapsize = mMapSize;
        if (mapsize <= (int)eMapSize.Small) return eMapSize.Small;
        else if (mapsize <= (int)eMapSize.Medium) return eMapSize.Medium;
        else if (mapsize <= (int)eMapSize.Large) return eMapSize.Large;
        return eMapSize.Large;
    }

    public List<string[]> GetMapDataFormatFromStringToList()
    {
        List<string[]> mapDataList = new List<string[]>();
        int mapSize = mMapSize;
        int index = 0;

        for (int i = 0;i < mapSize; i++)
        {
            string[] str = new string[mapSize];

            for (int j = 0; j < mapSize; j++)
            {
                DebugManager.Instance.Log("index " + index + " mapsize " + mapSize + " i " + i + " j " + j);
                string substring = mMapData.Substring(index, 1);

                if(substring == "9")
                {
                    str[j] = "000";
                    index += 1;
                }
                else
                {
                    str[j] = mMapData.Substring(index, 3);
                    index += 3;
                }
            }
            mapDataList.Add(str);
        }
        return mapDataList;
    }
}
