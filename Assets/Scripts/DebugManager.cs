﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

// デバッグに関するクラス
// リリース版のソースにログを残すときは必ずこのクラスのLog関数を使用する。
// そうすることによって、一元管理しやすくする。
// 現時点では、ユニティで実行されたときのみlogが出るようになっている


public class DebugManager {
	
	// インスタンス
	private static DebugManager mInstance;
	
	// インスタンス取得
	public static DebugManager Instance
	{
		get
		{
			return mInstance = mInstance == null ? new DebugManager() : mInstance;
		}
	}
	
		public enum eType{
			FATAL,	//	致命的なエラー
			ERROR,	//	エラー
			WARN,	//	警告
			INFO,	//	基幹部分の処理完了確認等
			DEBUG,	//	
			TRACE,	//	
		}

	/// <summary> ログを表示する </summary>
	public void Log(object log, eType type = eType.DEBUG)
	{
#if UNITY_EDITOR
		switch (type)
		{
			case eType.INFO:
				log = "<color=blue>" + log + "</color>";
				break;
		}
		//UnityEngine.Debug.Log("<color=blue>" + log + "</color>");
		UnityEngine.Debug.Log(log);
#endif
	}
	
	/// <summary> この関数が実行されたクラス名と関数名をログ表示 </summary>
	public void ClassMethodLog(int hierarchyLevel = 0)
	{
		// 引数の数だけ前に実行された場所の情報を取ってくる　+1しないとデフォルトでDebugManagerと表示されてしまう。
		StackFrame stackFrame = new StackFrame(hierarchyLevel + 1);
		
		string className  = stackFrame.GetMethod().ReflectedType.Name;
		string MethodName = stackFrame.GetMethod().Name;
		
		string log = "<color=green>" + className + "." + MethodName + " is executed !" + "</color>";
		
		Log(log);
	}
	
	/// <summary> 1次元リストの要素を表示 </summary>
	public string ArrayLog<T>(List<T> list,bool outFlg = true){
		
		string log = "{";
		
		for(int i = 0;i < list.Count;++i){
			log += list[i].ToString();
			log += i == list.Count - 1 ? "" : ",";
		}
		
		log += "}";
		
		if(outFlg){
			DebugManager.Instance.Log(log);
			return "";
		}
		return log;
	}

	/// <summary> 2次元リスト( List<string[]> )の要素を表示 </summary>
	public void StringsListLog(List<string[]> stringsList){
		
		string log = "";

		for (int lineIndex = 0; lineIndex < stringsList.Count; ++lineIndex)
		{
			string[] strings = stringsList[lineIndex];

			for (int colIndex = 0; colIndex < strings.Length; ++colIndex)
			{
				log += strings[colIndex];
				log += colIndex < strings.Length - 1 ? "," : "";
			}

			log += lineIndex < stringsList.Count - 1 ? "\n" : "";
		}
		
		Log(log);
	}
}
